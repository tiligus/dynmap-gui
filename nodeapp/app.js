const express = require('express')
const path = require('path')
const querystring = require('querystring');
const { spawn } = require('child_process');
const tiligus_utils =  require('./lib/tiligus-utils')
const app = express()
var shutdown_interval=0, proxy_id="",rm_mode=false,bash_finished=false

for(var i=0;i<process.argv.length;i++) (process.argv[i]=="--rm")?rm_mode=true:rm_mode

app.use('/', express.static(path.join(__dirname, '../www')))

app.get('/api?*', async (req, res) => {
    shutdown_interval=0;
    try{
        var parsed_data=querystring.parse(req.url.substr(5))
        if(parsed_data.getbash_finished){
            res.json({bash_finished});
        }else if(parsed_data.rmjar && rm_mode){
            bash_exec("./sh/rmjar.sh")
            bash_finished = false
            res.json({status:"running"})
        }else if(parsed_data.rmall && rm_mode){
            bash_exec("./sh/rmall.sh")
            bash_finished = false
            res.json({status:"running"})
        }else{
            res.json({ rm_mode, lang: req.headers.lang})
        }
    }catch(error){
        console.log(error)
    }
})

function bash_exec(command){
    const cmd = spawn(command);

    cmd.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`);
    });

    cmd.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });

    cmd.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
        bash_finished=true;
    });
}

async function start_webserver(){
    var available_port = await tiligus_utils.check_empty_ports(30000,50000); // find empty port in range 30000-50000
    if(!available_port) return console.log("No available ports"), process.exit(1)
    app.listen(available_port, async () => {
        proxy_id = await tiligus_utils.create_proxy(available_port,true)
        console.log("server running at port "+available_port)
    })
}
start_webserver()

setInterval(function(){
    shutdown_interval++
    if(shutdown_interval>(60*5)) return console.log("App not called, exiting..."), tiligus_utils.close_proxy(proxy_id,true)
}, 1000)