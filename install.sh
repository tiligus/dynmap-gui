#!/bin/bash
dynmap_legacy=/home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh
dynmap_wgui=/home/ubuntu/.scripts/plugins/pl-dynmap/remove.sh
node=/home/ubuntu/.nvm/versions/node/v14.15.5/bin/node

if test -f "$dynmap_legacy"; then
    echo "Legacy dynmap already installed, please remove it first" && exit
fi
if test -f "$dynmap_wgui"; then
    echo "Dynmap already installed." && exit
fi

sudo bash nginx_builder_install.sh # install nginx builder
echo "location /map/ {
            rewrite ^/map/(.*) /\$1 break;
            proxy_pass http://localhost:8123;
            proxy_set_header X-Real-IP \$remote_addr;
            proxy_set_header Host \$host\$uri;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_set_header X-NginX-Proxy true;
            proxy_redirect off;
            proxy_connect_timeout 10;
       }" | sudo tee -a /etc/nginx/builder/map.conf # create dynmap config for webserver
sudo /etc/nginx/builder/make_config.sh # build new nginx config
sudo mkdir /home/ubuntu/.scripts/plugins && sudo chmod 777 /home/ubuntu/.scripts/plugins
cp content/Dynmap-3.4-beta-2-spigot.jar /home/ubuntu/mc-server/plugins/Dynmap-3.4-beta-2-spigot.jar # copy plugin to plugin directory
mkdir /home/ubuntu/.scripts/plugins/pl-dynmap/
mv www/ /home/ubuntu/.scripts/plugins/pl-dynmap/www/
mv nodeapp/ /home/ubuntu/.scripts/plugins/pl-dynmap/nodeapp/
mv content/remove.sh $dynmap_wgui

curl -H "Content-type: application/json" -d '{"add_record":true, "name":"Dynmap 3.4-beta-2","remove_script":"/home/ubuntu/.scripts/plugins/pl-dynmap/remove.sh","file_0":"dynmap/configuration.txt","file_1":"dynmap/lightings.txt","file_2":"dynmap/markers.yml","file_3":"dynmap/perspectives.txt","file_4":"dynmap/shaders.txt","file_5":"dynmap/shaders.txt","file_6":"dynmap/worlds.txt"}' 'http://127.0.0.1:8181' # add plugin to panel

cd /home/ubuntu/.scripts/plugins/pl-dynmap/nodeapp/
$node app.js